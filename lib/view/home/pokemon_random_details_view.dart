import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class PokemonRandomDetailsView extends StatefulWidget {
  const PokemonRandomDetailsView({Key key}) : super(key: key);

  @override
  _PokemonRandomDetailsViewState createState() => _PokemonRandomDetailsViewState();
}

class _PokemonRandomDetailsViewState extends State<PokemonRandomDetailsView> {
  getPokemonData() async {
    final id = Random().nextInt(200);
    final response = await http.get(Uri.parse('https://pokeapi.co/api/v2/pokemon/$id'));
    final json = jsonDecode(response.body);

    final types = (json['types'] as List)
        .map((typeJson) => typeJson['type']['name'] as String)
        .toList();

    return {
      'id': json['id'],
      'name': json['name'],
      'imageUrl': json['sprites']['front_default'],
      'types': types,
    };
  }

  @override
  Widget build(BuildContext context) {
    // final details = getPokemonData();
    return Scaffold(
      appBar: AppBar(
        title: Text('Random'),
      ),
      backgroundColor: Color(0xFFFAFAFA),
      body: Center(
        child: FutureBuilder(
          future: getPokemonData(),
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return CircularProgressIndicator();
            }  else {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.network(
                    snapshot.data['imageUrl'],
                    scale: 0.5,
                  ),
                  Text(snapshot.data['name'].toUpperCase()),
                  Text('ID: ${snapshot.data['id']}'),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
