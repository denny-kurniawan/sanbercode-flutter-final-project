import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:sanbercodefinal/view/sign_in/signin_screen.dart';

class UserProfile extends StatelessWidget {
  const UserProfile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

    return Scaffold(
        appBar: AppBar(
          title: Text('Profile'),
        ),
        backgroundColor: Color(0xFFFAFAFA),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/img/Crest DeepPurple.png',
                scale: 7.5,
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                'Name',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 24.0,
                ),
              ),
              Text(
                _firebaseAuth.currentUser.email.split('@')[0],
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'Email',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 24.0,
                ),
              ),
              Text(
                _firebaseAuth.currentUser.email,
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              SizedBox(
                height: 50,
              ),
              ElevatedButton(
                // textColor: Colors.white,
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(100, 40),
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                ),
                child: Text("Sign out"),
                onPressed: () {
                  _firebaseAuth.signOut().then((value) => Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) => SignInScreen(),
                    ),
                  ));
                },
              ),
            ],
          ),
        ),
    );
  }
}
