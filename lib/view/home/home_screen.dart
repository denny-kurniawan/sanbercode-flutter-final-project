import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sanbercodefinal/bloc/nav_cubit.dart';
import 'package:sanbercodefinal/bloc/pokemon_bloc.dart';
import 'package:sanbercodefinal/bloc/pokemon_details_cubit.dart';
import 'package:sanbercodefinal/bloc/pokemon_event.dart';
import 'package:sanbercodefinal/view/home/app_navigator.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pokemonDetailsCubit = PokemonDetailsCubit();

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Evil Geniuses Pokemon',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        backgroundColor: Color(0xffFAFAFA),
      ),
      home: MultiBlocProvider(providers: [
        BlocProvider(
            create: (context) =>
            PokemonBloc()..add(PokemonPageRequest(page: 0))),
        BlocProvider(
            create: (context) =>
                NavCubit(pokemonDetailsCubit: pokemonDetailsCubit)),
        BlocProvider(create: (context) => pokemonDetailsCubit)
      ], child: AppNavigator()),
    );
  }
}
