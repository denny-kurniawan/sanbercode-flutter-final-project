import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:sanbercodefinal/view/home/pokemon_random_details_view.dart';
import 'package:sanbercodefinal/view/home/user_profile_view.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Padding(
        padding: const EdgeInsets.only(top: 24.0),
        child: ListView(
          children: <Widget>[
            Image.asset(
              'assets/img/Wordmark DeepPurple.png',
              height: 50,
            ),
            SizedBox(
              height: 20,
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PokemonRandomDetailsView()),
                );
              },
              title: Text(
                'Random',
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              dense: true,
              leading: Icon(
                Icons.shuffle,
                color: Color(0xFF673AB7),
                size: 32.0,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => UserProfile()),
                );
              },
              title: Text(
                'Profile',
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              dense: true,
              leading: Icon(
                Icons.person_rounded,
                color: Color(0xFF673AB7),
                size: 32.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
