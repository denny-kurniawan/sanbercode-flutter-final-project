import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sanbercodefinal/view/sign_in/signin_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Evil Geniuses Pokedex',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        backgroundColor: Color(0xffFAFAFA),
      ),
      home: SignInScreen(),
    );
  }
}
